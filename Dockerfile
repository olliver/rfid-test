#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

# For Alpine, latest is actually the latest stable
# hadolint ignore=DL3007
FROM registry.hub.docker.com/arm32v7/alpine:latest

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# We want the latest stable version from the repo
# hadolint ignore=DL3018
RUN \
    apk add --no-cache \
	binutils \
	gcc \
	linux-headers \
	make \
	musl-dev \
    && \
    rm -rf "/var/cache/apk/"*

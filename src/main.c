/*
 * main.c
 *
 * Brief: Main file for Communication board application
 *
 * Copyright (C) EvBox BV, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/* Includes
 * ---------------------------------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

#include "st_errno.h"
#include "platform.h"
#include "rfal_analogConfig.h"
#include "st25r3911_com.h"
#include "example_poller.h"
#include "EMC_Test.h"

/* Defines
 * ---------------------------------------------------------------------------------------------- */
#define IC_ID_REG_ADDRESS   0x3F
#define IC_ID_TYPE_MASK     0xF8
#define IC_ID_REV_MASK      0x07

static volatile sig_atomic_t keep_running = 1;

/* Private functions
 * ---------------------------------------------------------------------------------------------- */
static void print_help(char *cmd_name)
{
	fprintf(stdout, "Usage: %s [OPTIONS]\n", cmd_name);
	fprintf(stdout, "  -g N\t\t(REQUIRED)\tgpio number (dec) connected to the RFID_IRQ pin\n");
	fprintf(stdout, "  -d device\t(REQUIRED)\tspi to which the rfid chip is connected ie.: /dev/spidev0.0\n");
	fprintf(stdout, "  -c\t\t\t\tCheck chip presence\n");
	fprintf(stdout, "  -r 0xN\t\t\tRead register number N (hex)\n");
	fprintf(stdout, "  -w 0xN -v 0xV\t\t\tWrite value V (hex) to register number N (hex)\n");
	fprintf(stdout, "  -s\t\t\t\tTurn transmitter on with carrier wave only\n");
	fprintf(stdout, "  -o\t\t\t\tTurn transmitter on and transmit random data with OOK modulation\n");
	fprintf(stdout, "  -a\t\t\t\tTurn transmitter on and transmit random data with ASK modulation\n");
	fprintf(stdout, "  -p N\t\t\t\tEnter polling mode. N is the polling interval\n");
	fprintf(stdout, "  -n N\t\t\t\tIn polling mode, read N times. 0 < N < 128\n");
	fprintf(stdout, "  -h\t\t\t\tPrint this help and exit\n");
	fprintf(stdout, "\n");
}

static void sig_handler(int _)
{
	(void)_;
	keep_running = 0;
}
/* Public functions
 * ---------------------------------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	int ret;
	int opt;
	uint8_t read_reg;
	uint8_t read_val;
	uint8_t write_reg;
	uint8_t write_val;
	uint8_t ic_id;
	uint8_t ic_type;
	uint8_t ic_rev;
	uint8_t irq_gpio;
	int poll_interval;
	int8_t read_count = 0;
	int8_t max_reads;
	char *spidev;

	bool is_read;
	bool is_val;
	bool is_write;
	bool is_check;
	bool is_polling;
    bool is_carrier_only;
    bool is_ook;
    bool is_ask;
	char *p;

	is_read = false;
	is_write = false;
	is_val = false;
	is_check = false;
	is_polling = false;
	is_carrier_only = false;
	is_ook = false;
	is_ask = false;
	poll_interval = 20;
	max_reads = -1;

	ic_id = 0x0;
	ic_type = 0x0;
	ic_rev = 0x0;
	spidev = NULL;
	irq_gpio = 0xFF;

	signal(SIGINT, sig_handler);

	while ((opt = getopt(argc, argv, "g:d:w:r:v:soap:n:hc")) != -1) {
		switch(opt) {
		case 'd':
			spidev = optarg;
			break;
		case 'g':
			irq_gpio = (uint8_t)strtol(optarg, &p, 10);
			break;
		case 'r':
			is_read = true;
			read_reg = (uint8_t)strtol(optarg, &p, 16);
			break;
		case 'v':
			is_val = true;
			write_val = (uint8_t)strtol(optarg, &p, 16);
			break;
		case 'w':
			is_write = true;
			write_reg = (uint8_t)strtol(optarg, &p, 16);
			break;
		case 'p':
			is_polling = true;
			poll_interval = (int)strtol(optarg, &p, 10);
			break;
		case 'n':
			max_reads = (int8_t)strtol(optarg, &p, 10);
			break;
		case 'c':
			is_check = true;
			break;
		case 's':
			is_carrier_only = true;
			break;
        case 'o':
            is_ook = true;
            break;
        case 'a':
            is_ask = true;
            break;
		case 'h':
			print_help(argv[0]);
			exit(EXIT_SUCCESS);
		case '?':
			print_help(argv[0]);
			exit(EXIT_FAILURE);
		default:
			abort();
		}
	}

	if (spidev == NULL) {
		fprintf(stdout, "-d option is required\n");
		print_help(argv[0]);
		goto error;
	} else if (irq_gpio == 0xFF) {
		fprintf(stdout, "-g option is required\n");
		print_help(argv[0]);
		goto error;
	}

	ret = gpio_init(irq_gpio);
	if(ret != ERR_NONE)
		goto error;

	ret = spi_init(spidev);
	if(ret != ERR_NONE)
		goto error;

	ret = interrupt_init();
	if (ret != ERR_NONE)
		goto error;

	if (is_polling) {
        time_t current_time;
        time_t elapsed;
        int8_t n;

		exampleRfalPollerInit();
        current_time = time(NULL);
		while (keep_running) {
            n = read_count;
			exampleRfalPollerRun(poll_interval, &read_count);

            if (n == read_count) {
                elapsed = time(NULL) - current_time;
                if (elapsed > 5) {
                    keep_running = 0;
                }
            } else {
                current_time = time(NULL);
            }

			if (max_reads > 0 && read_count >= max_reads) {
				keep_running = 0;
			}
		}

		exampleRfalPollerDeInit();
		keep_running = 1;
	}

	if (is_carrier_only) {
		EMC_Test_Init(OOK);
		fprintf(stdout, "\nTransmitting carrier wave only.\n\n");
		while (keep_running) {}
		EMC_Test_DeInit();
		keep_running = 1;
	}

    if (is_ook) {
		EMC_Test_Init(OOK);
		fprintf(stdout, "\nTransmitting random data using OOK modulation.\n\n");
		while (keep_running) {
            EMC_Test_Run();
        }
		EMC_Test_DeInit();
		keep_running = 1;
    }

    if (is_ask) {
		EMC_Test_Init(ASK);
		fprintf(stdout, "\nTransmitting random data using ASK modulation.\n\n");
		while (keep_running) {
            EMC_Test_Run();
        }
		EMC_Test_DeInit();
		keep_running = 1;
    }

	st25r3911ReadRegister(IC_ID_REG_ADDRESS, &ic_id);

	ic_type = (ic_id & IC_ID_TYPE_MASK) >> 3;
	ic_rev = ic_id & IC_ID_REV_MASK;

	if (ic_id == 0 || ic_id == 0xFF) {
		fprintf(stdout, "\nRFID chip not found.\n\n");
		goto error;
	}

	if (is_check)
		fprintf(stdout, "\nRFID chip found.\nIC Type: 0x%X\nIC Revision: 0x%X\n\n",
				ic_type, ic_rev);

	if (is_read) {
		st25r3911ReadRegister(read_reg, &read_val);
		fprintf(stdout, "\nValue for register 0x%X is 0x%X\n\n",
				read_reg, read_val);
	}

	if (is_write) {
		if (!is_val) {
			fprintf(stdout, "\nMissing value to write\n\n");
			print_help(argv[0]);
		} else {
			st25r3911WriteRegister(write_reg, write_val);
			st25r3911ReadRegister(write_reg, &write_val);
			fprintf(stdout, "\nRegister 0x%X value has been set to 0x%X\n\n",
					write_reg, write_val);
		}
	}

	if (argc < 2)
		print_help(argv[0]);

	return EXIT_SUCCESS;

error:
	return EXIT_FAILURE;
}

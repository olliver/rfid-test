#include <stdlib.h>
#include <stdio.h>

#include "rfal_rf.h"
#include "rfal_analogConfig.h"
#include "st25r3911.h"
#include "EMC_Test.h"

static uint8_t txBuf[10];

void EMC_Test_Init(enum Modulation m)
{
    rfalAnalogConfigInitialize();
    rfalInitialize();
    st25r3911TxRxOn();

    if (m == OOK) {
        rfalSetMode(RFAL_MODE_LISTEN_ACTIVE_P2P, RFAL_BR_106, RFAL_BR_106);
    } else /* If not OOK it's ASK */ {
        rfalSetMode(RFAL_MODE_POLL_NFCB, RFAL_BR_106, RFAL_BR_106);
    }

    fprintf(stdout, "Initialized transmitter carrier\n");
}

void EMC_Test_DeInit()
{
    st25r3911TxRxOff();
    rfalDeinitialize();
    fprintf(stdout, "Transmitter turned off.\n");
}

void EMC_Test_Run()
{
    for (int i = 0; i < 10; i++)
        txBuf[i] = rand() % 255;

    rfalTransceiveBlockingTx(txBuf, 10, NULL, 0, NULL, RFAL_TXRX_FLAGS_DEFAULT, rfalConvMsTo1fc(20));
    rfalWorker();
}

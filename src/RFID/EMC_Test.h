#ifndef EMC_TEST_H
#define EMC_TEST_H

enum Modulation {
    OOK,
    ASK
};

void EMC_Test_Init(enum Modulation m);
void EMC_Test_DeInit();
void EMC_Test_Run();

#endif

#   ----------------------------------------------------------------------------
#  @file   Makefile
#
#  @desc   Makefile for RFID test tool (Debug and Release versions)
#
#   ----------------------------------------------------------------------------

#   ----------------------------------------------------------------------------
#   Included defined variables
#   ----------------------------------------------------------------------------
-include ../../Rules.make

#   ----------------------------------------------------------------------------
#   Variables passed in externally
#   ----------------------------------------------------------------------------
CFLAGS ?= -march=native \
	  -Wall -Wextra \
	  -Wnull-dereference \
	  -Wdouble-promotion \
	  -Wmissing-include-dirs \
	  -Wswitch-default \
	  -Wswitch-enum \
	  -Wuninitialized \
	  -Wstringop-truncation \
	  -Walloc-zero \
	  -Walloca \
	  -Wduplicated-branches \
	  -Wduplicated-cond \
	  -Wfloat-equal \
	  -Wdeclaration-after-statement \
	  -Wshadow \
	  -Wunsafe-loop-optimizations \
	  -Wbad-function-cast \
	  -Wcast-qual \
	  -Wcast-align \
	  -Wwrite-strings \
	  -Wconversion \
	  -Wjump-misses-init \
	  -Wlogical-op \
	  -Wmissing-prototypes \
	  -Wpacked \
	  -Wredundant-decls \
	  -Wnested-externs \
	  -Wlong-long \
	  -Wstack-protector \
	  -Wunsuffixed-float-constants
CROSS_COMPILE ?=

#   ----------------------------------------------------------------------------
#   Name of the Linux compiler
#   ----------------------------------------------------------------------------
CC ?= $(CROSS_COMPILE)gcc

#   ----------------------------------------------------------------------------
#   General options, sources and libraries
#   ----------------------------------------------------------------------------
NAME := rfidtest
SRCDIRS := src/	\
	src/RFID/ \

SRCS := $(notdir $(foreach SRCDIRS, $(SRCDIRS), $(shell find $(SRCDIRS) -maxdepth 1 -name '*.c')))
HDRS := $(foreach SRCDIRS, $(SRCDIRS), $(shell find $(SRCDIRS) -maxdepth 1 -name '*.h'))
vpath %.c $(SRCDIRS)
LIBS := pthread
BIN := rfidtest

#   ----------------------------------------------------------------------------
#   Compiler and Linker flags for Debug
#   ----------------------------------------------------------------------------
OBJDIR_D := Debug
BINDIR_D := $(OBJDIR_D)
LIBS_D := $(foreach LIBS, $(LIBS), -l$(LIBS))
OBJS_D := $(SRCS:%.c=$(OBJDIR_D)/%.o)
DEBUG_FLAGS := -O0 -g3 -gdwarf-2

#   ----------------------------------------------------------------------------
#   Compiler and Linker flags for Release
#   ----------------------------------------------------------------------------
OBJDIR_R := Release
BINDIR_R := $(OBJDIR_R)
LIBS_R := $(foreach LIBS, $(LIBS), -l$(LIBS))
OBJS_R := $(SRCS:%.c=$(OBJDIR_R)/%.o)
RELEASE_FLAGS := -O3 -Werror -Wdate-time

#   ----------------------------------------------------------------------------
#   All compiler options to be passed to the command line
#   ----------------------------------------------------------------------------
ALL_CFLAGS ?= -c                           \
              $(CFLAGS)

LDFLAGS ?=    -Wl,--hash-style=gnu

INC_PATHS := $(foreach SRCDIRS, $(SRCDIRS), -I$(abspath $(SRCDIRS))/)

#   ----------------------------------------------------------------------------
#   Compiler symbol definitions
#   For exp, DEFS := -DNONE
#   ----------------------------------------------------------------------------
DEFS :=

#   ----------------------------------------------------------------------------
#   Creating directory...
#   ----------------------------------------------------------------------------
$(OBJDIR_D):
	mkdir Debug

$(OBJDIR_R):
	mkdir Release

#   ----------------------------------------------------------------------------
#   Building Debug...
#   ----------------------------------------------------------------------------
debug: $(OBJDIR_D) $(BINDIR_D)/$(BIN)

$(BINDIR_D)/$(BIN): $(OBJS_D)
	@echo Compiling Debug...
	$(CC) -o $@ $(OBJS_D) $(LIBS_D) $(LDFLAGS) $(INC_PATHS) -Wl,-Map,$(BINDIR_D)/$(NAME).map
	@ln -sf "$@" "$(BIN)"

$(OBJDIR_D)/%.o : %.c $(HDRS)
	$(CC) $(DEBUG_FLAGS) $(DEFS) $(ALL_CFLAGS) $(INC_PATHS) -o$@ $<

#   ----------------------------------------------------------------------------
#   Building Release...
#   ----------------------------------------------------------------------------
release: $(OBJDIR_R) $(BINDIR_R)/$(BIN)

$(BINDIR_R)/$(BIN): $(OBJS_R)
	@echo Compiling Release...
	$(CC) -o $@ $(OBJS_R) $(LIBS_R) $(LDFLAGS) $(INC_PATHS) -Wl,-Map,$(BINDIR_R)/$(NAME).map
	@ln -sf "$@" "$(BIN)"

$(OBJDIR_R)/%.o : %.c $(HDRS)
	$(CC) $(DEFS) $(RELEASE_FLAGS) $(ALL_CFLAGS) $(INC_PATHS) -o$@ $<

.PHONY: clean
clean:
	@rm -fr $(OBJDIR_D)
	@rm -fr $(OBJDIR_R)
	@unlink $(BIN) || true

install: release
	@if [ ! -d "$(DESTDIR)" ] ; then \
		echo "DESTDIR ('$(DESTDIR)') is not a directory."; \
		exit 1; \
	fi
	@install -D -m 755 "$(BINDIR_R)/$(BIN)" "$(DESTDIR)/usr/bin/$(BIN)"
	@echo "RFID test tool Release version installed."


#!/bin/sh
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

WORKDIR="${WORKDIR:-/workdir}"
REQUIRED_COMMANDS="
    [
    basename
    command
    docker
    echo
    eval
    exit
    getopts
    hostname
    id
    nproc
    printf
    pwd
    readlink
    test
"


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

init()
{
    src_file="$(readlink -f "${0}")"
    src_dir="${src_file%%${src_file##*/}}"

    opt_env="${OPT_ENV:-}"
    opt_env="-e 'MAKEFLAGS=-j$(($(nproc) - 1))' ${opt_env}"
    opt_volume="-v '$(pwd):/${WORKDIR}' ${OPT_VOLUME:-}"

    CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-$(basename "$(pwd)"):latest}"

    if ! docker pull "${CI_REGISTRY_IMAGE}" 2> /dev/null; then
        e_warn "Unable to pull docker image '${CI_REGISTRY_IMAGE}', building locally instead."
        if ! docker build --pull -t "${CI_REGISTRY_IMAGE}" "${src_dir}"; then
            e_warn "Failed to build local container, attempting existing container."
        fi
    fi

    if ! docker inspect --type image "${CI_REGISTRY_IMAGE}" 1> /dev/null; then
        e_err "Container '${CI_REGISTRY_IMAGE}' not found, cannot continue."
        exit 1
    fi
}

run_script()
{
    eval docker run \
        --rm \
        -h "$(hostname)" \
        -i \
        -t \
        -u "$(id -u):$(id -g)" \
        -w "${WORKDIR}" \
        "${opt_env:-}" \
        "${opt_volume:-}" \
        "${CI_REGISTRY_IMAGE}" \
        "${@}"
}

check_requirements()
{
    for cmd in ${REQUIRED_COMMANDS}; do
        if ! test_result="$(command -V "${cmd}")"; then
            test_result_fail="${test_result_fail:-}${test_result}\n"
        else
            test_result_pass="${test_result_pass:-}${test_result}\n"
        fi
    done

    if [ -n "${test_result_fail:-}" ]; then
        e_err "Self-test failed, missing dependencies."
        echo "======================================="
        echo "Passed tests:"
        # As the results contain \n, we expect these to be interpreted.
        # shellcheck disable=SC2059
        printf "${test_result_pass:-none\n}"
        echo "---------------------------------------"
        echo "Failed tests:"
        # shellcheck disable=SC2059
        printf "${test_result_fail:-none\n}"
        echo "======================================="
        exit 1
    fi
}

main()
{
    check_requirements
    init

    if [ "${#}" -lt "1" ]; then
        run_script make release
    else
        run_script "${@}"
    fi
}

main "${@}"

exit 0
